import React, { Component } from 'react';
import Layout from './components/Layout/Layout'
import BurgerBuilder from './container/BurgerBuilder/BurgerBuilder'
import ScriptTag from 'react-script-tag';

class App extends Component {
  render() {
    return (
      <div >
        <Layout>
        <ScriptTag type="text/javascript" src='./germain/germainapm-uxmonitoring-loader.js'/>
          <BurgerBuilder/>
        </Layout>
       </div>
    );
  }
}

export default App;
