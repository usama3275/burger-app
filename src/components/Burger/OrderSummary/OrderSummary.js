import React from 'react'
//import Aux from '../../../hoc/Aux'

import Button from '../../UI/Button/Button'

const ordersummary = (props) => {
    const ingredientsummary = Object.keys(props.ingredient)
    .map(igKey=>{
                    return (
                                <li key={igKey}>
                                    <span style={{textTransform:'capitalize'}}>
                                        {igKey}:{props.ingredient[igKey]}
                                    </span>
                                </li>
                            )
    })
    return(
    <div>
        <h3>Your Order</h3>
        <p>A delicious burgur with the following Ingredients:</p>
        <ul>
            {ingredientsummary}
        </ul>
        <p><strong>Your Bill is:{props.price.toFixed(2)}$</strong></p>
        <p>Continue to Checkout?</p>
        <Button btnType ="Danger" clicked={props.purchaseCancelled}>Cancel</Button>
        <Button btnType ="Success" clicked={props.purchaseContinued}>Continue</Button>
    </div>
    

    )
};

export default ordersummary;