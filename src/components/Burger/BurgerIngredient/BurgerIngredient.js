import React ,{Component}from 'react';
import classes from './BurgerIngredient.css';
import PropTypes from 'prop-types';

class BurgerIngredient extends Component {
    render(){
        let ingtrdient=null;

    switch(this.props.type){
        case('bread-bottom'):
            ingtrdient=<div className={classes.BreadBottom}></div>;
            break;
        case('bread-top'):
            ingtrdient=(
                <div className={classes.BreadTop}>
                    <div className={classes.Seeds1}></div>
                    <div className={classes.Seeds2}></div>
                </div>
            );
            break;
        case('meat'):
                ingtrdient=<div className={classes.Meat}></div>;
                break;
        case('Cheese'):
                ingtrdient=<div className={classes.Cheese}></div>;
                break;
        case('bacon'):
                ingtrdient=<div className={classes.Bacon }></div>;
                break;     
        case('Salad'):
                ingtrdient=<div className={classes.Salad}></div>;
                break;
        default:
                ingtrdient=null;
    }
    return ingtrdient;

    }
}
    
BurgerIngredient.propTypes={
    type:PropTypes.string.isRequired
};


export default BurgerIngredient;