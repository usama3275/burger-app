import React, {Component} from 'react'
//import Aux from '../../hoc/Aux'

import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import Model from '../../components/UI/Model/Model'
import Ordersummary from '../../components/Burger/OrderSummary/OrderSummary'

const INGREDIENT_PRICES = {
    Salad:  0.5,
    Cheese: 1.3,
    meat:   0.7,
    bacon:  0.4

}


class BurgerBuiler extends Component{
    state = {
        ingredient:{
            Salad:0,
            bacon:0,
            Cheese:0,
            meat:0
        },
        totalPrice:4,
        purchasable:false,
        purchasing:false
    }
    upadatePurchaseState(ingredient){
        // const ingredient={
        //     ...this.state.ingredient
        // };
         const sum = Object.keys(ingredient).map(igKey=>{
            return ingredient[igKey]
        }).reduce((sum,el)=>{
            return sum+el;
        },0);
        this.setState({purchasable:sum>0});
    }
    addIngredientHandler = (type) => {
        const oldCount = this.state.ingredient[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = {
            ...this.state.ingredient
        };
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;
        this.setState({
            totalPrice: newPrice,
            ingredient: updatedIngredients
        });
        this.upadatePurchaseState(updatedIngredients);
     //   console.log(newPrice);
    }
    removeIngredientHandler = (type) => {
        const oldCount = this.state.ingredient[type];
        const updatedCount = oldCount - 1;
        const updatedIngredients = {
            ...this.state.ingredient
        };
        updatedIngredients[type] = updatedCount;
        const priceDeduction = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceDeduction;
        this.setState({
            totalPrice: newPrice,
            ingredient: updatedIngredients
        });
        this.upadatePurchaseState(updatedIngredients);

    }
    purchaseHandler = () =>{
        this.setState ({
            purchasing : true
        });
    }
    purchaseCancelHandler = () =>{
        this.setState ({
            purchasing : false
        });
    }
    purchaseContinueHandle =() =>{
        alert('Your Order is plased ');
    }
   
    render(){
        const disabledInfo = {
            ...this.state.ingredient
        };
        for(let key in disabledInfo){
            disabledInfo[key] = disabledInfo[key] <= 0
        }
        return(
            <div>
                <Model show={this.state.purchasing} modelClosed={this.purchaseCancelHandler}>
                    <Ordersummary
                         ingredient={this.state.ingredient}
                         purchaseCancelled= {this.purchaseCancelHandler}
                         purchaseContinued= {this.purchaseContinueHandle}
                         price={this.state.totalPrice}
                         //purchaseContinued= {this.purchaseContinueHandle}
                    /> 
                </Model>

                <Burger ingredient = {this.state.ingredient}/>
                <BuildControls
                 ingredientAdded = {this.addIngredientHandler}
                 ingredientRemoved = {this.removeIngredientHandler}
                 disabled = {disabledInfo}
                 price={this.state.totalPrice}
                 purchasable={this.state.purchasable}
                 ordered={this.purchaseHandler}
                />
            </div>
        );
    }
}

export default BurgerBuiler;