import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import ScriptTag from 'react-script-tag';

<ScriptTag type="text/javascript" src='./germain/germainapm-uxmonitoring-loader.js' />
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
